import cv2
import numpy as np
import torch
from torchvision.transforms import v2
import torchvision.transforms as transforms


class TorchImageProcessor:
    """Simple data processors."""

    def __init__(
        self,
        image_size,
        is_color,
        mean,
        scale,
        crop_size=0,
        pad=28,
        color="BGR",
        use_cutout=False,
        use_mirroring=False,
        use_random_crop=False,
        use_center_crop=False,
        use_random_gray=False,
    ):
        """Everything that we need to init."""
        self.augmentation = v2.RandomApply(torch.nn.ModuleList([
            v2.ToPILImage(),
            v2.Resize((112, 96)),
            v2.RandomPerspective(),
            v2.RandomAffine(),
            v2.RandomPhotometricDistort(),
            v2.GaussianBlur()
        ]))

        self.transforms = v2.Compose([
            v2.ToTensor(),
            v2.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

    def process(self, image_path):
        """Returns processed data."""
        
        try:
            image = cv2.imread(image_path)
        except:
            image = image_path

        if image is None:
            print(image_path)

        img = self.augmentation(img)
        img = self.transforms(img)
        
        return img
