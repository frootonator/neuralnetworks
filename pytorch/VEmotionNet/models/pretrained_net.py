import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision.models import resnet18

from pytorch.common.losses import *


class FeatureExtractor(nn.Module):
    def __init__(self, submodule, extracted_layers):
        super().__init__()
        self.submodule = submodule
        self.extracted_layers = extracted_layers

    def forward(self, data):
        x = data
        out = []
        for name, module in self.submodule._modules.items():
            if len(module._modules.items()) != 0:
                for name2, module2 in module._modules.items():
                    try:
                        x = module2(x)
                        if name2 in self.extracted_layers:
                            out.append(x)
                    except Exception as e:
                        print(e)
            else:
                try:
                    if name == 'fc5':
                        x = x.view(x.size(0), -1)
                    x = module(x)
                    if name in self.extracted_layers:
                        out.append(x)
                except Exception as e:
                    print(e)
        return out


class CNNNet(nn.Module):
    def __init__(self, num_classes, depth, data_size, emb_name=[], pretrain_weight=None):
        super().__init__()
        sample_size = data_size["width"]
        sample_duration = data_size["depth"]

        self.resnet = resnet18(pretrained=True)
        self.net = nn.Sequential()
        self.features = FeatureExtractor(self.resnet, list(self.resnet.children[-3]))
        self.net.add_module('resnet', self.features)
        self.net.add_module('fc1', nn.Linear(1000, 512))
        self.net.add_module('pr1', nn.ReLU())
        self.net.add_module('fc2', nn.Linear(512, num_classes))

    def forward(self, data):
        output = self.net(torch.squeeze(data, 2))
        return output
    
class C3D(nn.Module):
    def __init__(self):
        super(C3D, self).__init__()

        self.conv1 = nn.Conv3d(3, 64, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.pool1 = nn.MaxPool3d(kernel_size=(1, 2, 2), stride=(1, 2, 2))

        self.conv2 = nn.Conv3d(64, 128, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.pool2 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))

        self.conv3a = nn.Conv3d(128, 256, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.conv3b = nn.Conv3d(256, 256, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.pool3 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))

        self.conv4a = nn.Conv3d(256, 512, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.conv4b = nn.Conv3d(512, 512, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.pool4 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))

        self.conv5a = nn.Conv3d(512, 512, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.conv5b = nn.Conv3d(512, 512, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        self.pool5 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2), padding=(0, 1, 1))

        self.fc6 = nn.Linear(8192, 4096)
        self.fc7 = nn.Linear(4096, 4096)
        self.fc8 = nn.Linear(4096, 2)

        self.dropout = nn.Dropout(p=0.5)
        self.relu = nn.ReLU()

    def init_weight(self):
        for name, para in self.named_parameters():
            if name.find('weight') != -1:
                nn.init.xavier_normal_(para.data)
            else:
                nn.init.constant_(para.data, 0)

    def forward(self, x):
        h = self.relu(self.conv1(x))
        h = self.pool1(h)

        h = self.relu(self.conv2(h))
        h = self.pool2(h)

        h = self.relu(self.conv3a(h))
        h = self.relu(self.conv3b(h))
        h = self.pool3(h)

        h = self.relu(self.conv4a(h))
        h = self.relu(self.conv4b(h))
        h = self.pool4(h)

        h = self.relu(self.conv5a(h))
        h = self.relu(self.conv5b(h))
        h = self.pool5(h)

        h = h.view(-1, 8192)

        h = self.relu(self.fc6(h))
        h = self.dropout(h)

        h = self.relu(self.fc7(h))
        h = self.dropout(h)

        logits = self.fc8(h)

        return logits


class CNN3DNet(nn.Module):
    def __init__(self, softmax_size, depth, data_size, classnum=2, feature=False, emb_name=[],
                 pretrain_weight=None):
        super(CNNNet, self).__init__()

        self.C3Dnet = C3D()
        self.C3Dnet.init_weight()


    def forward(self, data):
        # tensor size: [batch_size, 3, depth, crop_size, crop_size]
        output = self.C3Dnet(torch.squeeze(data, 2))
        return output