import sys

sys.path.append("../")

import os
import pickle
import random

from pathlib import Path

import mlflow
import numpy as np

from accuracy import Accuracy
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from tqdm import tqdm

from pytorch.common.datasets_parsers.av_parser import AVDBParser


def get_data(dataset_root, file_list, max_num_clips=0, max_num_samples=50):
    dataset_parser = AVDBParser(
        dataset_root,
        os.path.join(dataset_root, file_list),
        max_num_clips=max_num_clips,
        max_num_samples=max_num_samples,
        ungroup=False,
        load_image=True,
    )
    data = dataset_parser.get_data()
    print("clips count:", len(data))
    print("frames count:", dataset_parser.get_dataset_size())
    return data


def calc_features(data):
    progresser = tqdm(
        iterable=range(0, len(data)),
        desc="calc video features",
        total=len(data),
        unit="files",
    )

    feat, targets = [], []
    for i in progresser:
        clip = data[i]

        rm_list = []

        for sample in clip.data_samples:
        #     dists_angles = []

        #     lm_src_inds = [37, 43, 27, 27, 39, 45, 41, 47, 33, 51, 48, 54, 66, 57, 54]
        #     lm_dst_inds = [19, 24, 21, 22, 36, 42, 37, 43, 29, 33, 4, 13, 62, 51, 48]

        #     for src in lm_src_inds:
        #         for dst in lm_dst_inds:
        #             Gx = sample.landmarks[src][0] - sample.landmarks[dst][0] + 1e-9
        #             Gy = sample.landmarks[src][1] - sample.landmarks[dst][1] + 1e-9
        #             dist = np.sqrt(np.power(Gx, 2) + np.power(Gy, 2))
        #             angle = np.arctan(np.divide(Gy, Gx))
        #             dists_angles.append(dist)
        #             dists_angles.append(angle)

        #     feat.append(dists_angles)

            dists_angles = []
            for i in range(len(sample.landmarks)):
                src = sample.landmarks[i]
                for j in range(i, len(sample.landmarks)):
                    dst = sample.landmarks[j]
                    Gx = src[0] - dst[0] + 1e-9
                    Gy = src[1] - dst[1] + 1e-9
                    dist = np.sqrt(np.power(Gx, 2) + np.power(Gy, 2))
                    angle = np.arctan(np.divide(Gy, Gx))
                    
                    dists_angles.append(dist)
                    dists_angles.append(angle)

            feat.append(dists_angles)
            targets.append(sample.labels)

        for sample in rm_list:
            clip.data_samples.remove(sample)

    print("feat count:", len(feat))
    return np.asarray(feat, dtype=np.float32), np.asarray(targets, dtype=np.float32)


def classification(X_train, X_test, y_train, y_test, accuracy_fn, pca_dim: int = 0):
    print(X_train.shape)
    if pca_dim > 0:
        pca = PCA(pca_dim)
        X_train = pca.fit_transform(X_train)
        X_test = pca.transform(X_test)

    # shuffle
    combined = list(zip(X_train, y_train))
    random.shuffle(combined)
    X_train[:], y_train[:] = zip(*combined)

    # TODO: используйте классификаторы из sklearn
    #############################################
    model = MLPClassifier(hidden_layer_sizes=(10, 25, 50, 25, 10), learning_rate_init=0.001,
                          learning_rate='adaptive', solver='adam', n_iter_no_change=5,
                          verbose=True, max_iter=1000)
    model.fit(X_train, y_train)
    #############################################

    y_pred = model.predict(X_test)
    accuracy_fn.by_frames(y_pred)
    accuracy_fn.by_clips(y_pred)


if __name__ == "__main__":
    experiment_name = "exp_5"
    max_num_clips = 0  # загружайте только часть данных для отладки кода
    use_dump = True  # используйте dump для быстрой загрузки рассчитанных фич из файла

    # dataset dir
    base_dir = Path("/home/radik/Projects/NeuralNetworks/data/")
    if 1:
        train_dataset_root = base_dir / "Ryerson/Video"
        train_file_list = base_dir / "Ryerson/train_data_with_landmarks.txt"
        test_dataset_root = base_dir / "Ryerson/Video"
        test_file_list = base_dir / "Ryerson/test_data_with_landmarks.txt"
    else:
        train_dataset_root = (
            base_dir / "OMGEmotionChallenge/omg_TrainVideos/frames"
        )
        train_file_list = (
            base_dir
            / "OMGEmotionChallenge/omg_TrainVideos/train_data_with_landmarks.txt"
        )
        test_dataset_root = (
            base_dir / "OMGEmotionChallenge/omg_ValidVideos/frames"
        )
        test_file_list = (
            base_dir
            / "OMGEmotionChallenge/omg_ValidVideos/valid_data_with_landmarks.txt"
        )

    if not use_dump:
        # load dataset
        train_data = get_data(train_dataset_root, train_file_list, max_num_clips=0)
        test_data = get_data(test_dataset_root, test_file_list, max_num_clips=0)

        # get features
        train_feat, train_targets = calc_features(train_data)
        test_feat, test_targets = calc_features(test_data)

        accuracy_fn = Accuracy(test_data, experiment_name=experiment_name)

        with open(experiment_name + '.pickle', 'wb') as f:
           pickle.dump([train_feat, train_targets, test_feat, test_targets, accuracy_fn], f, protocol=2)
    else:
        with open(experiment_name + ".pickle", "rb") as f:
            train_feat, train_targets, test_feat, test_targets, accuracy_fn = pickle.load(
                f
            )

    # run classifiers
            
    mlflow.sklearn.autolog(log_models=False, log_datasets=False)

    with mlflow.start_run():
        classification(
            train_feat,
            test_feat,
            train_targets,
            test_targets,
            accuracy_fn=accuracy_fn,
            pca_dim=25,
        )